package com.igorsharov.core_auth_impl.di.modules

import com.igorsharov.core_auth_api.domain.repositories.UserRepository
import com.igorsharov.core_auth_impl.UserRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal abstract class RepositoryModule {

    @Binds
    abstract fun userRepository(repository: UserRepositoryImpl): UserRepository
}