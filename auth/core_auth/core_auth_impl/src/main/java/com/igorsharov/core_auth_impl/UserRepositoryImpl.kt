package com.igorsharov.core_auth_impl

import com.igorsharov.core_auth_api.domain.repositories.UserModel
import com.igorsharov.core_auth_api.domain.repositories.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor() : UserRepository {

    override val user: UserModel
        get() = UserModel("Ivan", "Petrov")
}
