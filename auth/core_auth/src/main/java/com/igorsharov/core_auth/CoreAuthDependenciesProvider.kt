//package com.igorsharov.core_auth
//
//import com.igorsharov.core_auth_api.dependencies.CoreAuthDependencies
//import com.igorsharov.core_auth_impl.di.DaggerCoreAuthComponent
//import com.igorsharov.core_component_manager.holder.Holder
//import com.igorsharov.core_component_manager.holder.HoldersProvider
//import com.igorsharov.core_component_manager.injector.Injector
//import com.igorsharov.core_component_manager.injector.lifecycle.component.DomainComponentInjector
//
//object CoreAuthDependenciesProvider :
//    DomainComponentInjector<CoreAuthDependencies>(CoreAuthDependencies::class) {
//    override val holder: Holder = HoldersProvider.applicationHolder
//
//    override fun create(params: Injector<*, *>): CoreAuthDependencies =
//        DaggerCoreAuthComponent.builder().build()
//}
