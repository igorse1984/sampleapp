package com.igorsharov.core_auth_api.dependencies

import com.igorsharov.core_auth_api.domain.repositories.UserRepository

interface CoreAuthDependencies {

    fun provideUserRepository(): UserRepository
}
