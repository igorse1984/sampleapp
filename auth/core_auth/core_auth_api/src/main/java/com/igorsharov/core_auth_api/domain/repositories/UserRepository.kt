package com.igorsharov.core_auth_api.domain.repositories

interface UserRepository {

    val user: UserModel
}
