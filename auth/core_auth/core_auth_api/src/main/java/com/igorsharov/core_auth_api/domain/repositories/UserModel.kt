package com.igorsharov.core_auth_api.domain.repositories

data class UserModel(
    val firstName: String,
    val lastName: String
)
