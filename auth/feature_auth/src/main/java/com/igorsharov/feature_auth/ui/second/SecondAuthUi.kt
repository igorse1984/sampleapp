package com.igorsharov.feature_auth.ui.second

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.igorsharov.core_navigation.screen.FirstAuthDestination
import com.igorsharov.core_navigation.screen.GeneralDestination
import com.igorsharov.feature_auth.R

@Composable
fun SecondAuthScreen(
    navController: NavController,
    viewModel: SecondAuthViewModel = hiltViewModel()
) {
    var inputText: String by rememberSaveable { mutableStateOf("") }
    val localFocusManager = LocalFocusManager.current
    Text(
        text = viewModel.userName,
        modifier = Modifier.padding(24.dp)
    )
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        TextField(
            value = inputText,
            label = { Text(stringResource(id = R.string.second_auth_screen_hint)) },
            onValueChange = {
                inputText = it
            },
            modifier = Modifier
                .fillMaxWidth(),
            singleLine = true,
            keyboardActions = KeyboardActions(
                onDone = {
                    localFocusManager.clearFocus()
                }
            )
        )
        Box(
            modifier = Modifier
                .padding(top = 24.dp)
        ) {
            Button(
                onClick = {
//                    viewModel.onButtonClicked()
                    navController.navigate(GeneralDestination.createRoute(inputText)) {
                        popUpTo(FirstAuthDestination.route()) {
                            inclusive = true
                        }
                    }
                },
                enabled = inputText.isNotEmpty()
            ) {
                Text(
                    text = stringResource(id = R.string.second_auth_screen_button)
                )
            }
        }
    }
}