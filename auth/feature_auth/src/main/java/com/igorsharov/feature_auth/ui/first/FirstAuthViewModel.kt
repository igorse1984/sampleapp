package com.igorsharov.feature_auth.ui.first

import androidx.lifecycle.ViewModel
import com.igorsharov.core_navigation.GlobalNavigator
import com.igorsharov.core_navigation.screen.SecondAuthDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FirstAuthViewModel @Inject constructor() : ViewModel() {

    fun onAuthButtonClicked() {
        GlobalNavigator.navigate(SecondAuthDestination.route())
    }
}
