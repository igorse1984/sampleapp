package com.igorsharov.feature_auth.ui.first

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import com.igorsharov.feature_auth.R

@Composable
fun FirstAuthScreen(
    viewModel: FirstAuthViewModel = hiltViewModel()
) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Button(
            onClick = {
                viewModel.onAuthButtonClicked()
            }
        ) {
            Text(
                text = stringResource(id = R.string.first_auth_screen_button)
            )
        }
    }
}