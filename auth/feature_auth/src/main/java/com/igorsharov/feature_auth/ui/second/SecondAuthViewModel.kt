package com.igorsharov.feature_auth.ui.second

import androidx.lifecycle.ViewModel
import com.igorsharov.core_auth_api.domain.repositories.UserRepository
import com.igorsharov.core_navigation.GlobalNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SecondAuthViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val userName: String
        get() = "${userRepository.user.firstName} ${userRepository.user.lastName}"

    fun onButtonClicked() {
        //TODO
    }
}
