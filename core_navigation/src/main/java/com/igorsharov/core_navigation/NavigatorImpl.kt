package com.igorsharov.core_navigation

import androidx.navigation.NavHostController
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
internal class NavigatorImpl @Inject constructor(
    private val navController: NavHostController
) : Navigator {

    override fun navigate(route: String) {
        navController.navigate(route)
    }

    override fun navigateUp() {
        navController.navigateUp()
    }

    override fun popBack() {
        navController.popBackStack()
    }

    override fun popUpTo(route: String, inclusive: Boolean) {
        navController.popBackStack(route, inclusive)
    }
}
