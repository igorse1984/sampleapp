package com.igorsharov.core_navigation

import androidx.navigation.NavHostController
import kotlin.properties.Delegates

object GlobalNavigator {

    private var navigator: Navigator by Delegates.notNull()

    fun init(navController: NavHostController) {
        navigator = NavigatorImpl(navController)
    }

    fun navigate(route: String) {
        navigator.navigate(route)
    }

    fun navigateUp() {
        navigator.navigateUp()
    }
}

interface Navigator {
    fun navigate(route: String)

    fun navigateUp()

    fun popBack()

    fun popUpTo(
        route: String,
        inclusive: Boolean = false
    )
}