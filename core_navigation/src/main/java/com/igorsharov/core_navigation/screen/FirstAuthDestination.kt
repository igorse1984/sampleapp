package com.igorsharov.core_navigation.screen

import androidx.navigation.NavHostController
import com.igorsharov.core_navigation.NavigationDestination

object FirstAuthDestination : NavigationDestination {

    override fun route(): String = "firstAuthScreen"
}