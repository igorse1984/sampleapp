package com.igorsharov.core_navigation.screen

import com.igorsharov.core_navigation.NavigationDestination

object SecondAuthDestination : NavigationDestination {

    override fun route(): String = "secondAuthScreen"
}