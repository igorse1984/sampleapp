package com.igorsharov.core_navigation.screen

import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.igorsharov.core_navigation.NavigationDestination

object GeneralDestination : NavigationDestination {

    const val ID_PARAM = "id"
    private const val ROUTE = "generalScreen"

    override val arguments: List<NamedNavArgument>
        get() = listOf(
            navArgument(ID_PARAM) { type = NavType.StringType }
        )

    override fun route(): String = "$ROUTE/{$ID_PARAM}"

    fun createRoute(id: String) = "$ROUTE/$id"
}