package com.igorsharov.sampleapplication.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.igorsharov.core_navigation.GlobalNavigator
import com.igorsharov.core_navigation.screen.FirstAuthDestination
import com.igorsharov.core_navigation.screen.GeneralDestination
import com.igorsharov.core_navigation.screen.SecondAuthDestination
import com.igorsharov.feature_auth.ui.first.FirstAuthScreen
import com.igorsharov.feature_auth.ui.second.SecondAuthScreen
import com.igorsharov.feature_general_screen.GeneralScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController: NavHostController = rememberNavController()
            GlobalNavigator.init(navController)
            SetupNavHost(navController)
        }
    }

    @Composable
    private fun SetupNavHost(navController: NavHostController) {
        NavHost(
            navController,
            FirstAuthDestination.route()
        ) {
            composable(FirstAuthDestination.route()) {
                FirstAuthScreen()
            }
            composable(SecondAuthDestination.route()) {
                SecondAuthScreen(navController)
            }
            composable(
                route = GeneralDestination.route(),
                arguments = GeneralDestination.arguments
            ) {
                GeneralScreen(it.arguments?.getString(GeneralDestination.ID_PARAM))
            }
        }
    }
}
