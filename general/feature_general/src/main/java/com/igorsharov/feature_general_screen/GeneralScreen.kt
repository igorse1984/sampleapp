package com.igorsharov.feature_general_screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.igorsharov.feature_primary_screen.R

@Composable
fun GeneralScreen(
    id: String?
) {
    Text(
        text = stringResource(id = R.string.general_screen_header),
        modifier = Modifier.padding(24.dp)
    )
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        val text = stringResource(id = R.string.general_screen_text, id ?: "")
        Text(
            text = text,
            textAlign = TextAlign.Center
        )
    }
}